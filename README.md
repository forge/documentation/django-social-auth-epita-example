social-auth-backend-epita Django example project
================================================

This Django project shows how to use the social-auth-backend-epita package to
authenticate users with the EPITA SSO service provided by the CRI.

## Introduction

The EPITA SSO service is based on the OpenID Connect (OIDC) technology. In
order to use this example project you need to get a set of credentials from the
CRI. Each set of parameters is tied to an OIDC *client*:

  1. `ACCESS_KEY`
  2. `SECRET_KEY`

An OIDC client can request access to *scopes*, each one giving access to
specific *claims*. For instance, the `phone` scope gives the client access to
the `phone_number` and `phone_number_verified` claims for each user connecting
to your service with your OIDC client.

You may use the public OIDC test client provided by the CRI, this client is
allowed to request access to any scope.

The public test client can only be used during developement since the
`redirect_uri` parameter is tied to `localhost`, you will need to request a
specific client for your production setup. You may do so by sending a mail to
tickets@cri.epita.fr with your `redirect_uri` and the needed scopes.

## Configuration

You will need Docker and docker-compose installed on your machine.

Type the following commands and enter the relevant OIDC client credentials :

```sh
$ cd docker/
$ ./gen_secrets.sh
Please input a value for django-oidc-access-key: ACCESSKEYEXAMPLE
Please input a value for django-oidc-secret-key: SECRETKEYEXAMPLE
````

## Accessing the example website

Use the following commands to start the example website :

```sh
$ cd docker/
$ DOCKER_BUILDKIT=1 COMPOSE_DOCKER_CLI_BUILD=1 docker-compose up -d --build
Creating network "docker_default" with the default driver
Creating docker_db_1 ... done
Creating docker_django_social_auth_example_1 ... done
Creating docker_db_adminer_1                 ... done
Creating docker_reverse_1                    ... done
```

Then go to http://localhost:8000/ with your web browser.
